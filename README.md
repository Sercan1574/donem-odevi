using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Sercan_Çetin_B181200026_Hal_Kayıt_Tutucu
{
    class Program
    { 
        
        static void Main(string[] args)
        {  
            int kaç = 1;
            string kontrol;
            string kontrol1;
            int i = 1;

            string fatura_dosyası = "fatura_defteri.txt";

            StreamReader dosyaoku = new StreamReader (fatura_dosyası);
            string satır;
            List<fatura_okuyucu> kayıt = new List<fatura_okuyucu>();
            do {
                satır = dosyaoku.ReadLine();
                string[] sütunal = satır.Split(';');

                kayıt.Add(new fatura_okuyucu { 
                
                künye=int.Parse(sütunal[0]),
                cins=sütunal[1],
                kilo=double.Parse(sütunal[2]),
                adet=int.Parse(sütunal[3]),
                fiyat=double.Parse(sütunal[4]),
                }); ;

               
            i++;
  
            
            } while (!dosyaoku.EndOfStream);
             dosyaoku.Close();

            int k = 1;
          //ana menü ve seçenekler.....
           StreamWriter sw = new StreamWriter("ekli_fatura.txt");

            Console.WriteLine("/****************************************************************************\n" +
                    "**                SAKARYA ÜNİVERSİTESİ \n" +
                    "**       BİLGİSAYAR VE BİLİŞİM BİLİMLERİ FAKÜLTESİ\n" +
                    "**        BİLİŞİM SİSTEMLERİ MÜHENDİSLİĞİ BÖLÜMÜ\n" +
                    "**             NESNEYE DAYALI PROGRAMLAMA DERSİ\n" +
                    "**                  2019-2020 BAHAR DÖNEMİ \n" +
                    "**                                          \n" +
                    "**           ÖDEV NUMARASI..........:01 \n" +
                    "**       ÖĞRENCİ ADI............:SERCAN ÇETİN \n" +
                    "**       ÖĞRENCİ NUMARASI.......:B181200026\n" +
                    "**           DERSİN ALINDIĞI GRUP...:1 \n" +
                    "****************************************************************************/ ");
            do
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("                                                        ANA MENÜ                 \n                                                   Kayıtlı Fatura '{0}'   .\n                                            Kapatmak ve Kaydetmek  için Press 'c' \n                                                Yeni Fatura için Press 'n'\n                                                 faturalarda gezmek için's' ", (i-1));
                
                kontrol = Console.ReadLine();


                if (kontrol == "n")
                {


                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\tKünye\tCins\tKilo\tAdet\tFiyat  /.............Her Sütün arasına '.' Konulmalı;En Son 'Enter' Basılmalı/");
          //kaydederken belirlenen opsyonlar
                    string toy = Console.ReadLine();
                    string[] tut = toy.Split(".");

                   

                    kayıt.Add(new fatura_okuyucu
                        {

                            künye = int.Parse(tut[0]),
                            cins = tut[1],
                            kilo = double.Parse(tut[2]),
                            adet = int.Parse(tut[3]),
                            fiyat = double.Parse(tut[4]),
                            zaman = Convert.ToString(DateTime.Now),
                        }); ;
                    
                    foreach (var item in kayıt) {
                        sw.WriteLine("{0}.  \t{1}   \t{2}   \t{3}   \t{4}   \t{5} ", k, item.künye, item.cins, item.kilo, item.adet, item.fiyat);
                        
                    }
                    

                    Console.ReadKey();
                    i++;


                }
                if (kontrol == "s")
                {
                    do
                    {

                        Console.Clear();
                       
                    Console.Write("Kayıtlı Fatura Sayısı'{0}'\nKaç Numaralı Faturaya Bakıyorsun:", (kaç - 1));
                        
                    Console.Clear();
                    Console.Write("Kayıtlı Fatura Sayısı'{0}'\nKaç Numaralı Faturaya Bakıyorsun:", (kaç - 1));
                    Console.Clear();
                    Console.WriteLine("No.\tKünye\t    Cins\t|\tKilo\tAdet\tFiyat\tTutar");

                        k = 1;
                        List<fatura_okuyucu> SortedList = kayıt.OrderByDescending(x => x.fiyat).ToList();
                        Console.ForegroundColor = ConsoleColor.Blue;
                        foreach (var item in SortedList) { 
                        Console.WriteLine("{0}.\t{1}\t    {2}\t|\t{3}\t{4}\t{5}\t{6}.TL\t{7} ", k, item.künye, item.cins, item.kilo, item.adet, item.fiyat,item.kilo*item.fiyat,item.zaman);
                            
                            k++;
                                }
                        Console.ReadKey();
                    
                        
                        
                       
                        Console.WriteLine("                           Ana Dönmek İçin Press cc\n                                 Devam etmek için enter.");

                        kontrol1 = Console.ReadLine();
                        Console.Clear();

                    } while (kontrol1 != "cc");
                }
                






            } while (kontrol != "c");

            sw.Close();

        }
    }
}
